#include "mainwindow.h"

#include <QApplication>
#include <QPushButton>
#include <QFont>
#include <QObject>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QPushButton quit("Quit");

    quit.setVisible(true);
    quit.resize(140, 70);

    QObject::connect(&quit, SIGNAL(clicked()), &a, SLOT(quit()));

    return a.exec();
}
