#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Cod custom.
    ui->groupBox_2->setVisible(true);
    ui->groupBox_3->setVisible(true);
    ui->groupBox_2->show();
    ui->groupBox_3->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked() {
    // Femeie.
    if (ui->radioButton->isChecked()) {
        if(ui->radioButton_3->isChecked()) {
            // Fumator.
            ui->textEdit->setText("Esti o femeie fumatoare.");
        } else {
            // Nefumator.
             ui->textEdit->setText("Esti o femeie nefumatoare.");
        }

    } else {
        // Barbat.
        if(ui->radioButton_3->isChecked()) {
            // Fumator.
            ui->textEdit->setText("Esti un barbat fumator.");
        } else {
            // Nefumator.
             ui->textEdit->setText("Esti un barbat nefumator.");
        }
    }

    // Arata textul nou.
    ui->textEdit->show();
}

void MainWindow::resizeEvent(QResizeEvent *event) {
//    ui->pushButton->setGeometry(0, 0, width()/2, height()/2);
    ui->groupBox->setGeometry(0, 0, width(), height());
}

