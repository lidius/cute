#include <QApplication>
#include <QWidget>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QMainWindow>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QMainWindow * window = new QMainWindow;
    QMenuBar * menuBar = new QMenuBar;
    QMenu * fileMenu = new QMenu("&File", window);
    QMenu * otherMenu2 = new QMenu("&Others", window);
    QAction * helpAbout = fileMenu->addAction("&Help");
    QAction * exitAction = fileMenu->addAction("Close");
    menuBar->addMenu(fileMenu);
    menuBar->addMenu(otherMenu2);
    QObject::connect(exitAction, SIGNAL(triggered()), &app,
    SLOT(quit()));
    QObject::connect(helpAbout, SIGNAL(triggered()), &app,
    SLOT(aboutQt()));
    window->setMenuBar(menuBar);
    window->show();
    return app.exec();
}
